<!DOCTYPE html>
<html lang="en">
    <!-- begin::Head -->
    <head>
        <!-- Meta -->
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">

        <!-- Title -->
        <title><?php echo $title ?></title>

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="./assets/css/bootstrap-4.4.1/bootstrap.min.css">

        <!-- Font Awesome -->
        <script src="https://kit.fontawesome.com/54dd48c648.js" crossorigin="anonymous"></script>

        <!-- External CSS -->
        <link rel="stylesheet" href="./assets/css/style.css">

    </head>
    <!-- end::Head -->
    <!-- begin::Body -->
    <body>