<?php
    // Starting session
    session_start();

    // Destroying session
    session_destroy();

    // Redirect to index page
    header("location: ../index.php?login=logout_success");
?>