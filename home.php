<?php
    // Starting session
    session_start();

    if (!isset($_SESSION['email']) ){
        header('location: index.php?login=access_denied');
    }

    if( isset( $_GET['message'] ) ){

        $response = $_GET['message'];

        if ( $response == "addSuccess" ){
            $response = "<div class='alert alert-success alert-dismissible fade show' role='alert'>Your task is added successfully!
                            <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                <span aria-hidden='true'>&times;</span>
                            </button>
                        </div>";
        }

        if ( $response == "deleteSuccess" ){
            $response = "<div class='alert alert-success alert-dismissible fade show' role='alert'>Your task is successfully deleted!
                            <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                <span aria-hidden='true'>&times;</span>
                            </button>
                        </div>";
        }

        if ( $response == "updateSuccess" ){
            $response = "<div class='alert alert-success alert-dismissible fade show' role='alert'>Your task is successfully updated!
                            <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                <span aria-hidden='true'>&times;</span>
                            </button>
                        </div>";
        }

        if ( $response == "failed" ){
            $response = "<div class='alert alert-warning alert-dismissible fade show' role='alert'>Connection Failed! 
                            <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                <span aria-hidden='true'>&times;</span>
                            </button>
                        </div>";
        }


    }else{
        $response = "";
    }

    $page = 'home';

    $title = 'Home Page';
    include_once 'includes/head.php';
    include_once 'includes/navbar.php';
?>
        
    <div class="container">
        <div class="col">
            <form method="POST" action="includes/add_task_list_action.php" class="mt-5">
                <?php
                    echo $response;
                ?>
                <div class="input-group mb-2">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Input Your Task</div>
                    </div>
                    <input type="text" class="form-control" name="task" placeholder="Ex: Read Bible" required> 
                    <button type="submit" class="btn btn-primary"><i class="fas fa-plus-circle"></i></button>
                </div>
            </form>
        </div>
        <div class="col">
            <?php

                include_once 'includes/db_connect.php';

                $uid = $_SESSION['uid'];
                
                $sql = "SELECT * FROM `tasks` WHERE user_id = $uid ";

                $result = mysqli_query($conn, $sql);

                echo '<table class="table">
                        <thead>
                        <tr>
                            <th scope="col" class="text-center">#</th>
                            <th scope="col">Task</th>
                            <th scope="col" class="text-center">Action</th>
                        </tr>
                        </thead>
                        <tbody>';

                if( mysqli_num_rows($result) > 0 ){
                    
                    $count = 1; 
                    while( $row = mysqli_fetch_assoc($result) ){

                        echo '<tr>
                                <td width="5%" class="text-center">' . $count .'</td>
                                <td width="80%">' . $row['task_name'] . '</td>
                                <td width="15%" class="text-center"><button class="btn btn-success" data-toggle="modal" data-target="#editTask' . $row['task_id'] . '"><i class="fas fa-edit"></i></button> <a class="btn btn-danger" href="includes/delete_record.php?taskID=' . $row['task_id'] . '"><i class="fas fa-trash-alt"></i></a></td>
                            </tr>';

                        $count++;

                        echo '<div class="modal fade" id="editTask' . $row['task_id'] . '" aria-hidden="true">
                            <div class="modal-dialog modal-md">
                                <div class="modal-content">
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <form method="POST" action="includes/edit_task_list_action.php" class="mt-3">
                                                    <div class="form-group">
                                                        <div class="input-group mb-2">
                                                            <div class="input-group-prepend">
                                                                <div class="input-group-text">Edit</div>
                                                            </div>
                                                            <input type="hidden" name="uid" value="' . $row['task_id'] . '">
                                                            <input type="text" class="form-control" name="task" value="' . $row['task_name'] . '" required> 
                                                            <input type="submit" value="Update" class="btn btn-primary">
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>';
                    }

                    echo '</tbody></table>';

                }else{
                    echo '<tr>
                            <td colspan="3" class="text-center">*** No Records Found ***</td>
                          </tr>';
                }
            ?>
        </div>

        

    </div>
        
<?php
    include_once 'includes/script.php';
?>
